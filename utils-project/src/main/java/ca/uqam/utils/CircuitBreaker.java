package ca.uqam.utils;

import java.util.*;
import java.lang.*;

import ca.uqam.utils.interfaces.IMicroServiceClient;
import ca.uqam.utils.exceptions.*;

import org.slf4j.*;

public class CircuitBreaker<T,V extends IMicroServiceClient<T>> {
	private boolean open;
	private V mcClient;
	private int attemptThreshold;
	private int callCount;
	private long retryTime;

	private final Logger log = LoggerFactory.getLogger(CircuitBreaker.class);

	public T executeMCCall(Map<String, String> allRequestParams){
		if(!this.open)
			return call(allRequestParams);
		else
			return this.mcClient.fallback();
	}

	private T call(Map<String, String> allRequestParams){
		try{
			return this.mcClient.executeRequest(allRequestParams);
		} catch(MicroServiceFailedException e){
			if(this.callCount < this.attemptThreshold){
				this.callCount++;
				return call(allRequestParams);
			} else{
				this.callCount = 0;
				this.open = true;
				return recover();
			}
		}
		
	}

	private T recover(){
		log.error("MC " + this.mcClient.getMicroServiceBaseURI() + " failed. opening circuitbreaker");
		Thread thread = new Thread(tryCall);
		thread.start();
		return this.mcClient.fallback();
	}

	private Runnable tryCall = () -> {
		try{
			while(this.open){
				Thread.sleep(retryTime * 1000);	//Problematique
				this.open = !this.mcClient.testRequest();
			}
			log.info("MC " + this.mcClient.getMicroServiceBaseURI() + " run succeded");
		} catch (InterruptedException e){
			Thread.currentThread().interrupt();
		}
	};

	public CircuitBreaker(V mcClient){
		this.mcClient = mcClient;
		this.attemptThreshold = 5;
		this.retryTime = 5;
	}

	/**
	 * @param mcClient Microservice Client that implements IMicroServiceClient interface
	 * @param attemptThreshold Number of attempts before the circuit breaks open
	 * @param retryTime Number of seconds the object should wait before retrying the service
	 */
	public CircuitBreaker(V mcClient, int attemptThreshold, long retryTime){
		this.mcClient = mcClient;
		this.attemptThreshold = attemptThreshold;
		this.retryTime = retryTime;
	}
}
