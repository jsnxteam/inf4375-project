package ca.uqam.utils.resources;

import com.fasterxml.jackson.annotation.*;
import java.io.*;

public class Location{
	private int idLieu;

	@JsonProperty("nom")
	private String nom;

	@JsonProperty("lat")
	private double lat;

	@JsonProperty("lng")
	private double lng;

	@Override
	public String toString(){
		return String.format("%s : (%f, %f)", nom, lat, lng);
	}

	public Location(String nom, double lat, double lng){
		this.nom = changeToUTF8(nom);
		this.lat = lat;
		this.lng = lng;
	}

	public Location(double lat, double lng){
		this.nom = "undefined";
		this.lat = lat;
		this.lng = lng;
	}

	public Location(int idLieu, String nom, double lat, double lng){
		this.idLieu = idLieu;
		this.nom = changeToUTF8(nom);
		this.lat = lat;
		this.lng = lng;
	}

	public Location(){}

	private String changeToUTF8(String toChange){
		byte[] bStr;
		String fStr = "";
		try{
			bStr = toChange.getBytes("ISO-8859-1");
			fStr = new String(bStr, "UTF-8");
		}catch(UnsupportedEncodingException e){}
		return fStr;
	}
	public int 	getIdLieu() { return this.idLieu; }
	@JsonProperty public String getNom() { return this.nom; }
	@JsonProperty public double getLat() { return this.lat; }
	@JsonProperty public double getLng() { return this.lng; }
	public String getPointPostGis() { return String.format("POINT(%f %f)",this.getLat(), this.getLng()); }
}
