package ca.uqam.utils;

import java.util.*;
import ca.uqam.utils.exceptions.*;

import ca.uqam.utils.CircuitBreaker;
import ca.uqam.utils.interfaces.*;

public class Main {
	public static void main(String... args) throws MicroServiceFailedException {
		CircuitBreaker<String, Test> cb = new CircuitBreaker(new Test());
	}
}

class Test implements IMicroServiceClient<String> {
	private boolean failswitch = true;

	public String executeRequest(Map<String, String> allRequestParams) throws MicroServiceFailedException{
		if(failswitch)
			throw new MicroServiceFailedException(this);
		return "OK";
	}

	public boolean testRequest(){
		return false;
	}

	public String fallback(){
		return "okTest";
	}

	public String getMicroServiceBaseURI(){
		return "baseURI";
	}
}
