package ca.uqam.utils.exceptions;

import ca.uqam.utils.interfaces.IMicroServiceClient;

public class MicroServiceFailedException extends Exception {
	private final String message = "";
	private IMicroServiceClient msc;

	public MicroServiceFailedException(IMicroServiceClient msc){
		this.msc = msc;
	}

	@Override
	public String getMessage(){
		return message + msc.getMicroServiceBaseURI();
	}
}