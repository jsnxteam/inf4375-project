package ca.uqam.utils.repositories;

import java.util.*;
import java.util.stream.*;
import java.sql.*;

import ca.uqam.utils.resources.*;
import ca.uqam.utils.interfaces.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.support.*;
import org.springframework.stereotype.*;

@Component
public class LocationRepository implements IRepository<Location> {
	@Autowired private JdbcTemplate jdbcTemplate;

	private static final String INSERT_STMT =
	"insert into Lieu (nom, lat, lng) " +
	"values (?, ?, ?) " +
	"on conflict do nothing"
	;

	public int insert(Location lieu){
		return jdbcTemplate.update(conn -> {
			PreparedStatement ps = conn.prepareStatement(INSERT_STMT);
			ps.setString(1, lieu.getNom());
			ps.setDouble(2, lieu.getLat());
			ps.setDouble(3, lieu.getLng());
			return ps;
		});
	}

	private static final String FIND_ALL_STMT =
	"select * from Lieu";

	public List<Location> selectAll() {
		return jdbcTemplate.query(FIND_ALL_STMT, new LocationRowMapper());
	}

	private static final String FIND_BY_CONTENT_STMT =
	"select * from Lieu " +
	"where nom = ? AND " +
		  "lat = ? AND " +
	 	  "lng = ?"
	;

	public Location selectByContent(String nom, double lat, double lng) {
		return checkDataIntegrity(jdbcTemplate.query(FIND_BY_CONTENT_STMT, new Object[]{nom, lat, lng}, new LocationRowMapper()));
	}

	private static final String FIND_BY_ID_STMT =
	"select * from Lieu " +
	"where idLieu = ?"
	;

	public Location selectById(int id){
		return checkDataIntegrity(jdbcTemplate.query(FIND_BY_ID_STMT, new Object[]{id}, new LocationRowMapper()));
	}

	public Location checkDataIntegrity(List<Location> data){
		if(		data.isEmpty()) return null;
		else if(data.size() == 1) return data.get(0);
		else {
			throw new org.springframework.dao.EmptyResultDataAccessException(1); //Throws exception with expected size of 1
		}
	}

	private static final String TRUNCATE_STMT =
	"truncate table lieu cascade"
	;

	public void truncate()
	{
		jdbcTemplate.execute(TRUNCATE_STMT);
	}

	class LocationRowMapper implements RowMapper<Location> {
		public Location mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Location(
				rs.getInt("idLieu"),
				rs.getString("nom"),
				rs.getDouble("lat"),
				rs.getDouble("lng")
			);
		}
	}
}
