package ca.uqam.utils.interfaces;

import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;

 /**
  * Repository Interface
  * Ensures that select queries that should return only one element throw exceptions accorgingly
  */
public interface IRepository<T>{

	/**
	 * applies to select queries that should return only one row.
	 * @return T object if only one row is selected or null if no row is selected
	 * @throws EmptyResultDataAccessException(1) if more than one row is returned
	 */
	public T checkDataIntegrity(List<T> data) throws EmptyResultDataAccessException;

	public List<T> 	selectAll();
	public int 		insert(T elem);
}