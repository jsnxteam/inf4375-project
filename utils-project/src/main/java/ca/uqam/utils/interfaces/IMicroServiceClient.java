package ca.uqam.utils.interfaces;

import java.util.*;
import ca.uqam.utils.exceptions.MicroServiceFailedException;

public interface IMicroServiceClient<T>{
	/**
	 * How to execute the MC call
	 * @return MC return object
	 */
	public T executeRequest(Map<String, String> allRequestParams) throws MicroServiceFailedException;

	/**
	 * Tries to execute and placegolder MC call
	 * @return success call or failure call
	 */
	public boolean testRequest();

	/**
	 * what to do on failure call from executeRequest()
	 */
	public T fallback();

	public String getMicroServiceBaseURI();
}