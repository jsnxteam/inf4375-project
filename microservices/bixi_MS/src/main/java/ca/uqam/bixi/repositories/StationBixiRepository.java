package ca.uqam.bixi.repositories;

import java.util.*;
import java.util.stream.*;
import java.sql.*;

import ca.uqam.utils.repositories.*;
import ca.uqam.utils.interfaces.*;
import ca.uqam.bixi.resources.*;
import ca.uqam.utils.resources.*;


import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.support.*;
import org.springframework.stereotype.*;

@Component
public class StationBixiRepository implements IRepository<StationBixi> {
	@Autowired private JdbcTemplate jdbcTemplate;
	@Autowired private LocationRepository locationRepository;

	//private LocationRepository locationRepository = new LocationRepository();

	private static final String INSERT_STMT =
	"insert into Station_Bixi (idBixi, nom, idTerminal, etat, bloquee, suspendue, hors_service, MAJ_Depuis, COM_Depuis, idLieu, bornes_disponibles, bornes_indisponibles, velos_disponibles, velos_indisponibles)" +
	"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " +
	"on conflict do nothing"
	;

	public int insert(StationBixi station){
		return jdbcTemplate.update(conn -> {
			int idLieu = handleLocation(station.getLieu());

			PreparedStatement ps = conn.prepareStatement(INSERT_STMT);
			ps.setInt(1, station.getId());
			ps.setString(2, station.getNom());
			ps.setInt(3, station.getIdTerminale());
			ps.setInt(4, station.getEtat());
			ps.setBoolean(5, station.isBloquee());
			ps.setBoolean(6, station.isSuspendue());
			ps.setBoolean(7, station.isHorsService());
			ps.setLong(8, station.getMajDepuis());
			ps.setLong(9, station.getComDepuis());
			ps.setInt(10, idLieu);
			ps.setInt(11, station.getBornesDisponibles());
			ps.setInt(12, station.getBornesIndisponibles());
			ps.setInt(13, station.getVelosDisponibles());
			ps.setInt(14, station.getVelosIndisponibles());

			return ps;
		});
	}

	//checks wether or not location exists in the db, if not inserts it.
	private int handleLocation(Location lieu){
		Location dbLieu = locationRepository.selectByContent(lieu.getNom(), lieu.getLat(), lieu.getLng());
		if(dbLieu == null){
			locationRepository.insert(lieu);
			dbLieu = locationRepository.selectByContent(lieu.getNom(), lieu.getLat(), lieu.getLng()); //TODO: Refactor that
		}

		return dbLieu.getIdLieu();
	}

	private static final String FIND_ALL_STMT =
	"select * from Station_Bixi"
	;

	public List<StationBixi> selectAll(){
		return jdbcTemplate.query(FIND_ALL_STMT, new StationBixiRowMapper());
	}

	private static final String FIND_BY_ID_STMT =
	"select * from station_bixi " +
	"where idStation = ?"
	;

	public StationBixi selectById(int id) {
    	return checkDataIntegrity(jdbcTemplate.query(FIND_BY_ID_STMT, new Object[]{id}, new StationBixiRowMapper()));
  	}

	public StationBixi checkDataIntegrity(List<StationBixi> data){
		if(		data.isEmpty()) return null;
		else if(data.size() == 1) return data.get(0);
		else throw new org.springframework.dao.EmptyResultDataAccessException(1); //Throws exception with expected size of 1
	}

	private static final String TRUNCATE_STMT =
	"truncate table station_bixi cascade"
	;

	public void truncate() {
		jdbcTemplate.execute(TRUNCATE_STMT);
		locationRepository.truncate();
	}

	private static final String FIND_BY_RADIUS =
	"select * from station_bixi "+
	"inner join Lieu on station_bixi.idLieu = lieu.idLieu "+
	"where ST_DWithin(lieu.geo, ST_MakePoint( ? ,?), ?, false) "+
	"and velos_disponibles >= ?"
	;
	public List<StationBixi> selectByRadius(int dispo, double lat, double lng, double radius){
		return jdbcTemplate.query(FIND_BY_RADIUS,
			new PreparedStatementSetter() {
				  public void setValues(PreparedStatement ps) throws
					SQLException {
						ps.setDouble(1,lat);
						ps.setDouble(2,lng);
						ps.setDouble(3,radius);
						ps.setInt(4,dispo);
					}
			},
			new StationBixiRowMapper()
		);
	}

	class StationBixiRowMapper implements RowMapper<StationBixi> {
		public StationBixi mapRow(ResultSet rs, int rowNum) throws SQLException {
			Location lieu = locationRepository.selectById(rs.getInt("idLieu"));
			if(lieu != null){
				return new StationBixi(
					rs.getInt("idBixi"),
					rs.getString("nom"),
					rs.getInt("idTerminal"),
					rs.getInt("etat"),
					rs.getBoolean("bloquee"),
					rs.getBoolean("suspendue"),
					rs.getBoolean("hors_service"),
					rs.getLong("maj_depuis"),
					rs.getLong("com_depuis"),
				    lieu.getLat(),
    				lieu.getLng(),
    				rs.getInt("bornes_disponibles"),
    				rs.getInt("bornes_indisponibles"),
    				rs.getInt("velos_disponibles"),
    				rs.getInt("velos_indisponibles"),
   					lieu
				);
			} else return null;
		}
	}
}
