package ca.uqam.bixi.controllers;

import java.util.*;
import java.io.*;
import java.sql.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import ca.uqam.bixi.repositories.*;
import ca.uqam.bixi.resources.*;
import ca.uqam.bixi.tasks.*;

import java.nio.charset.StandardCharsets;

import org.slf4j.*;

@Controller
public class StationBixiController{

    @Autowired private StationBixiRepository stationRepository;
    @Autowired private FetchBixiData bixiFetcher;

    private final Logger log = LoggerFactory.getLogger(StationBixiController.class);

    private static final String INCORRECT_DISPO = "Value provided for min_bixi_dispo is incorrect.";
    private static final String INCORRECT_LAT = "Value provided for lat is incorrect.";
    private static final String INCORRECT_LNG = "Value provided for lng is incorrect.";
    private static final String INCORRECT_RADIUS = "Value provided for radius is incorrect.";
    private static final String MISSING_LAT = "Latitude needs to be provided alongside the longitude.";
    private static final String MISSING_LNG = "Longitude needs to be provided alongside the latitude.";



    @RequestMapping("/stations-bixi/id/{id}")
    public ResponseEntity selectStationById(@PathVariable("id") int id) throws JsonProcessingException {
        try{
            StationBixi station = stationRepository.selectById(id);
            if(station == null)
                return ResponseEntity.notFound().build();
            return ResponseEntity.ok(serializeStation(station));
        } catch(Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }
    }

    @RequestMapping(value = {"/stations-bixi", "/stations-bixi/"}, method = RequestMethod.GET)
    public ResponseEntity getStations(@RequestParam Map<String, String> allRequestParams){
       try{
           return findByRadius(allRequestParams);
       } catch(Exception e){
           log.error(e.getMessage());
           return ResponseEntity.status(500).build();
       }

   }

   public ResponseEntity findByRadius(@RequestParam Map<String,String> allRequestParams){
       double radius = 1000, lat = 45.508762, lng = -73.568780;
       int dispo = 0;
       byte[] data;

       if(allRequestParams.get("min_bixi_dispo") != null){
           try{
               dispo = Integer.parseInt(allRequestParams.get("min_bixi_dispo"));
           } catch(NumberFormatException e){
               return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(INCORRECT_DISPO);
           }
       }

       if(allRequestParams.get("rayon") != null){
           try{
               radius = Double.parseDouble(allRequestParams.get("rayon"));
           } catch(NumberFormatException e){
               return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(INCORRECT_RADIUS);
           }
       }

       if(allRequestParams.get("lat") == null && allRequestParams.get("lng") != null)
           return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(MISSING_LAT);
       else if(allRequestParams.get("lat") != null && allRequestParams.get("lng") == null)
           return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(MISSING_LNG);
       else if(allRequestParams.get("lat") != null && allRequestParams.get("lng") != null){
           try {
               lat = Double.parseDouble(allRequestParams.get("lat"));
           } catch(NumberFormatException e){
               return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(INCORRECT_LAT);
           }

           try {
               lng = Double.parseDouble(allRequestParams.get("lng"));
           } catch(NumberFormatException e){
               return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(INCORRECT_LNG);
           }
       }

       data = search(dispo, lat, lng, radius);
       return ResponseEntity.ok(new String(data, StandardCharsets.UTF_8));
   }
   private byte[] search(int dispo, double lat, double lng, double radius){
       byte[] data;
       List<StationBixi> stations = stationRepository.selectByRadius( dispo, lat, lng, radius);
       ByteArrayOutputStream out = new ByteArrayOutputStream();
       ObjectMapper mapper = new ObjectMapper();
       try{
           mapper.writeValue(out, stations);
       }catch(IOException e){}

       data = out.toByteArray();
       return data;
   }

    private String serializeStation(StationBixi station) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(station);
    }

    //manualy fetch bixi stations
    @RequestMapping(value={"/stations-bixi/updater"}, method = RequestMethod.POST)
    public ResponseEntity updateActivities(Model model) throws ProcessingException, IOException {
        try{
            bixiFetcher.execute();
            return ResponseEntity.ok("OK");
        } catch (Exception e){
            log.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }
    }
}
