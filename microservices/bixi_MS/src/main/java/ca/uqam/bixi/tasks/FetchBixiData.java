package ca.uqam.bixi.tasks;

import ca.uqam.bixi.resources.*;
import ca.uqam.bixi.repositories.*;
import ca.uqam.utils.JsonSchemaHandler;

import java.util.*;
import java.util.stream.*;

import java.io.*;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ArrayNode;

import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.scheduling.annotation.*;
import org.springframework.web.client.*;
import org.springframework.http.converter.*;

import java.nio.charset.*;

import org.slf4j.*;

@Component
public class FetchBixiData {

  	private final Logger log = LoggerFactory.getLogger(FetchBixiData.class);

	private static final String URL = "https://secure.bixi.com/data/stations.json";
	String stationSchemaResource = "resource:/JSON-Schema/StationBixi-Schema.json";

	@Autowired private StationBixiRepository stationRepository;

	@Scheduled(cron="0 */10 * * * *") // toutes les 10 minutes
	public void execute() throws ProcessingException, IOException {
		try {
			RestTemplate rt = new RestTemplate();
			rt.getMessageConverters()
		  	  .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

		  	final String json = rt.getForObject(URL, String.class);

			final JsonNode urlRep = (ArrayNode) JsonLoader.fromString(json).get("stations");
			JsonSchemaHandler jsh = new JsonSchemaHandler(stationSchemaResource);

			final JsonNode processedData = jsh.validate(urlRep.deepCopy());

	    	ObjectMapper mapper = new ObjectMapper();
	    	List<StationBixi> stations = mapper.readValue(processedData.toString(), mapper.getTypeFactory()
	        																           .constructCollectionType(List.class, StationBixi.class));

            stationRepository.truncate();

			for(StationBixi station : stations){
				station.finishSerialization();
				stationRepository.insert(station);
			}

			log.info("Received Json data from "+URL);
		} catch(ProcessingException e){
			log.error("Received invalid Json Format from : "+URL+"\n"+e.getMessage());
		} catch(java.lang.ClassCastException e) {
			log.error("Received invalid Json Format from : "+URL+"\n"+e.getMessage());
		}
	}
}
