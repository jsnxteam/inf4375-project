var cron = require('node-cron');
var fetch = require('node-fetch');
var Ajv = require('ajv');
var ajv = new Ajv();
var bixiRepo = require('../repositories/bixi_repository.js');

var schema = require('./StationBixi-Schema.json');
var validator = ajv.compile(schema);

var cronFetchStations = cron.schedule('0 */10 * * * *', function(){
	fetchStations((resp) => resp);
}, true);

var fetchStations = function (callback){
	fetch('https://secure.bixi.com/data/stations.json')
		.then((resp) => resp.json())
		.then((resp) => {
			var report = validator(resp.stations);
			if(!report) console.log(validator.errors);
			else{
				bixiRepo.truncate((res) =>{
					for(var i = 0; i < resp.stations.length; i++){
						bixiRepo.insert(
							resp.stations[i].id,
							resp.stations[i].s,
							resp.stations[i].n,
							resp.stations[i].st,
							resp.stations[i].b,
							resp.stations[i].su,
							resp.stations[i].m,
							resp.stations[i].lu,
							resp.stations[i].lc,
							`Station ${resp.stations[i].s}`,
							resp.stations[i].la,
							resp.stations[i].lo,
							resp.stations[i].da,
							resp.stations[i].dx,
							resp.stations[i].ba,
							resp.stations[i].bx		
						);
					}
				});	
				console.log('Reveceived JSON data from https://secure.bixi.com/data/stations.json');
			}
			callback("OK");			
		});
}

module.exports = {fetchStations:fetchStations};