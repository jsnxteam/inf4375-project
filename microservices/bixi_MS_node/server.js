var express = require('express'),
	bodyParser = require('body-parser'),
	fetchBixi = require('./tasks/fetch_stations_bixi.js'),
	bixiController = require('./controller/bixi_controller.js'),
	serverPORT = 8082;

var app = express();

app.set('port', (process.env.PORT || serverPORT));
app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : true }));

app.get('/stations-bixi', function(req, res, cb){
    bixiController.getStations(req.query, function(result){
		res.header("Content-Type", "application/json; charset=utf-8");
    	if(result instanceof Error)
    		res.status(422).send(result.message);
    	else{
    		res.send(result);
    	}
    	return cb();
    });
});

app.get('/stations-bixi/:id', function(req, res, cb){
	bixiController.getStationbyId(req.params.id, (results) => {
		res.header("Content-Type", "application/json; charset=utf-8");
		if(results instanceof Error)
			res.status(500).send();
		else if(results.rows.length == 0)
			res.status(404).send();
		else
			res.send(results.rows);
		return cb();
	});
});

app.post('/stations-bixi/updater', function(req, res, cb){
	fetchBixi.fetchStations((resp) => {
		res.send(resp);
		return cb();
	});
});

app.listen(app.get('port'), function() {
    console.log("server is listening on %s", app.get('port')); 
});