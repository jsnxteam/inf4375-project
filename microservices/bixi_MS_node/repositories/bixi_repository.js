var pg = require('pg');
var lieuRepository = require('./lieu_repository.js');

var config = {
  user: 'postgres',
  password: 'postgres',
  host: 'localhost',
  port: 5432,
  database: 'inf4375_bixi',
  client_encoding: 'utf8'
};

var client = new pg.Client(config);
client.connect();

// queries ######################################
var INSER_STMT = {
    name: 'insert station',
    text: "insert into Station_Bixi (idBixi, nom, idTerminal, etat, bloquee, suspendue, hors_service, MAJ_Depuis, COM_Depuis, idLieu, bornes_disponibles, bornes_indisponibles, velos_disponibles, velos_indisponibles)" +
	"values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14) " +
	"on conflict do nothing",
    values: []
};

var TRUNCATE_STMT = {
	name: "truncate station",
	text: "truncate table station_bixi cascade"
};

var FIND_BY_RADIUS_STMT = {
	name: "select by radius",
	text: "select * from station_bixi "+
		  "inner join Lieu on station_bixi.idLieu = lieu.idLieu "+
	      "where ST_DWithin(lieu.geo, ST_MakePoint( $1 ,$2), $3, false) "+
	      "and velos_disponibles >= $4",
	values:[]
};

var FIND_BY_ID_STMT = {
	name: "select by id",
	text: "select * from station_bixi "+
		  "where idStation = $1",
    values:[]
};
// ##############################################

module.exports = {
	insert: function(idBixi, nom, idTerminal, etat, bloquee, suspendue, hors_service, MAJ_Depuis, COM_Depuis, nom, lat, lieu, bornes_disponibles, bornes_indisponibles, velos_disponibles, velos_indisponibles){
		lieuRepository.findByContent(nom, lat, lieu, (res) => {
			var idLieu;
			if(res.rows.length > 1) throw Error("Selected rows from Lieu > 1");
			else if (res.rows.length == 0){
				lieuRepository.insert(nom, lat, lieu, (res) => {
					lieuRepository.findByContent(nom, lat, lieu, (res) => {
						idlieu = res.rows[0].idlieu;
						insertStation(idBixi, nom, idTerminal, etat, bloquee, suspendue, hors_service, MAJ_Depuis, COM_Depuis, idlieu, bornes_disponibles, bornes_indisponibles, velos_disponibles, velos_indisponibles);
					});
				});
			} else {
				idlieu = res.rows[0].idlieu;
				insertStation(idBixi, nom, idTerminal, etat, bloquee, suspendue, hors_service, MAJ_Depuis, COM_Depuis, idlieu, bornes_disponibles, bornes_indisponibles, velos_disponibles, velos_indisponibles);
			}
		});
	},

	truncate : function(callback){
		client.query(TRUNCATE_STMT, (err, res) => {
			if(err) throw err;
			lieuRepository.truncate((res) => {callback(res);});
		});
	},
	findByRadius: function(lat, lng, radius, dispo, callback){
		var findstmt = clone(FIND_BY_RADIUS_STMT);
		findstmt.values = [lat, lng, radius, dispo];
		client.query(findstmt, (err, res) => {
			if(err) throw err;
			callback(mapRowsToStation(res.rows));
		});
	},
	findById: function(id, callback){
		var findstmt = clone(FIND_BY_ID_STMT);
		findstmt.values = [id],
		client.query(findstmt, (err, res) => {
			if(err) throw err;
			callback(mapRowsToStation(res.rows));
		});
	},
	end: function(){
		client.end();
	}
};

function insertStation (idBixi, nom, idTerminal, etat, bloquee, suspendue, hors_service, MAJ_Depuis, COM_Depuis, idLieu, bornes_disponibles, bornes_indisponibles, velos_disponibles, velos_indisponibles){
	var insertstmt = clone(INSER_STMT);
	insertstmt.values = [idBixi, nom, idTerminal, etat, bloquee, suspendue, hors_service, MAJ_Depuis, COM_Depuis, idLieu, bornes_disponibles, bornes_indisponibles, velos_disponibles, velos_indisponibles];
	client.query(insertstmt, (err, res) => {
		if(err) throw err;
	});
}

//clone javascript objects
function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

function mapRowsToStation(rows){
	var mappedObject = [];
	for(var i = 0; i < rows.length; i++){
		mappedObject[i] = {};
		mappedObject[i].id = rows[i].idstation;
		mappedObject[i].s = rows[i].nom;
		mappedObject[i].n = rows[i].idterminal;
		mappedObject[i].st = rows[i].etat;
		mappedObject[i].b = rows[i].bloquee;
		mappedObject[i].su = rows[i].suspendue;
		mappedObject[i].horsService = rows[i].hors_service;
		mappedObject[i].majDepuis = rows[i].maj_depuis;
		mappedObject[i].comDepuis = rows[i].com_depuis;
		mappedObject[i].lieu = {
			idLieu:rows[i].idlieu,
			nom: `Station ${rows[i].nom}`,
			lat:rows[i].lat,
			lng:rows[i].lng
		}
		mappedObject[i].bornesDisponibles = rows[i].bornes_disponibles;
		mappedObject[i].bornesIndisponibles = rows[i].bornes_indisponibles;
		mappedObject[i].velosDisponibles = rows[i].velos_disponibles;
		mappedObject[i].velosIndisponibles = rows[i].velos_indisponibles;
		mappedObject[i].m = rows[i].hors_service;
		mappedObject[i].lu = rows[i].maj_depuis;
		mappedObject[i].lc = rows[i].com_depuis;
		mappedObject[i].la = rows[i].lat;
		mappedObject[i].lo = rows[i].lng;
		mappedObject[i].da = rows[i].bornes_disponibles;
		mappedObject[i].dx = rows[i].bornes_indisponibles;
		mappedObject[i].ba = rows[i].velos_disponibles;
		mappedObject[i].bx = rows[i].velos_indisponibles;
		mappedObject[i].bk = false;
		mappedObject[i].bl = false;
	}

	return mappedObject;
}