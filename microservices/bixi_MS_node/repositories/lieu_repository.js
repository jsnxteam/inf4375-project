var pg = require('pg');
var conString = "postgres://postgres:postgres@localhost:5432/inf4375_bixi";

var client = new pg.Client(conString);
client.connect();

// queries ######################################
var INSERT_STMT = {
	name: 'insert lieu',
	text: "insert into Lieu (nom, lat, lng) " +
		  "values ($1, $2, $3) " +
		  "on conflict do nothing",
	values: []
};

var TRUNCATE_STMT = {
	name: 'truncate lieu',
	text: "truncate table lieu cascade"
};

var FIND_BY_ID = {
	name: 'find by id',
	text: "select * from Lieu " +
		  "where idLieu = $1",
	values: []
}

var FIND_BY_CONTENT = {
	name: 'find by content',
	text: "select * from Lieu " +
		  "where nom = $1 AND " +
		  "lat = $2 AND " +
	 	  "lng = $3",
	values: []
}

// ##############################################


module.exports = {
	insert: function(nom, lat, lng, callback){
		var insertstmt = clone(INSERT_STMT);
		insertstmt.values = [nom, lat, lng];
		client.query(insertstmt, (err, res) => {
			if(err) throw err;
			callback(res);
		});
	},
	truncate: function(callback){
		client.query(TRUNCATE_STMT, (err, res) =>{
			if(err) throw err;
			callback(res);
		});
	},
	findById: function(id, callback){
		var findByIdstmt = clone(FIND_BY_ID);
		findByIdstmt.values = [id];
		client.query(findByIdstmt, (err, res) => {
			if(err) throw err;
			callback(res);
		})
	},
	findByContent: function(nom, lat, lng, callback){
		var findByContentstmt = clone(FIND_BY_CONTENT);
		findByContentstmt.values = [nom, lat, lng];
		client.query(findByContentstmt, (err, res) => {
			if(err) throw err;
			callback(res);
		});
	},
	end:function(){
		client.end();
	}
};

//clone javascript objects
function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}