var bixiRepo = require('../repositories/bixi_repository.js');
var bixifetcher = require('../tasks/fetch_stations_bixi.js');

var INCORRECT_DISPO = "Value provided for min_bixi_dispo is incorrect.";
var INCORRECT_LAT = "Value provided for lat is incorrect.";
var INCORRECT_LNG = "Value provided for lng is incorrect.";
var INCORRECT_RADIUS = "Value provided for radius is incorrect.";
var MISSING_LAT = "Latitude needs to be provided alongside the longitude.";
var MISSING_LNG = "Longitude needs to be provided alongside the latitude.";


module.exports = {
	getStations: function(params, callback){
		try{
			params = validateParams(params);
			search(params["lat"], params["lng"], params["rayon"], params["min_bixi_dispo"], (results) => {
				callback(results);	
			});
		} catch (err){
			console.log(err);
			callback(err);
		}
	},
	getStationbyId: function(id, callback){
		try{
			if(Number(id)){
				bixiRepo.findById(id, (results) => {
					callback(results);
				});
			}
		} catch(err){
			callback(err);
		}
	}
}

function validateParams(params){
	if(params["min_bixi_dispo"]){
		if(!(Number(params["min_bixi_dispo"]) && params["min_bixi_dispo"]))
			throw Error(INCORRECT_DISPO);
	} else{
		params["min_bixi_dispo"] = 0;
	}
	if(!(params["rayon"] && params["rayon"] >= 0))
		throw Error(INCORRECT_RADIUS);
	if(!(params["lat"] && Number(params["lat"])))
		throw Error(INCORRECT_LAT);
	if(!(params["lng"] && Number(params["lng"])))
		throw Error(INCORRECT_LNG);
	return params;
}

function search(lat, lng, radius, dispo, callback){
	bixiRepo.findByRadius(lat, lng, radius, dispo, (results) => {
		callback(results);
	});
}