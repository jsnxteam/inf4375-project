CREATE TABLE Lieu(
    idLieu SERIAL NOT NULL,
    nom TEXT NOT NULL,
    lat DOUBLE PRECISION,
    lng DOUBLE PRECISION,
    PRIMARY KEY (idLieu, nom),
    UNIQUE (idLieu)
);

SELECT AddGeometryColumn('lieu','geo',4326,'POINT',2);

CREATE TABLE Activity(
    idActivity INTEGER PRIMARY KEY UNIQUE NOT NULL,
    nom TEXT NOT NULL,
    description TEXT,
    arrondissement TEXT,
    dates DATE[],
    idLieu SERIAL REFERENCES Lieu (idLieu) NOT NULL
);