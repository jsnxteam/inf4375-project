package ca.uqam.activities.repositories;

import java.util.*;
import java.util.stream.*;
import java.sql.*;

import ca.uqam.utils.repositories.*;
import ca.uqam.utils.interfaces.*;
import ca.uqam.activities.resources.*;
import ca.uqam.utils.resources.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.support.*;
import org.springframework.stereotype.*;

@Component
public class ActivityRepository implements IRepository<Activity> {
	@Autowired private JdbcTemplate jdbcTemplate;
	@Autowired private LocationRepository locationRepository;

	private static final String INSERT_STMT =
	"insert into activity (idActivity, nom, description, arrondissement, dates, idLieu)" +
	"values (?, ?, ?, ?, ?, ?)" +
	"on conflict do nothing"
	;

	public int insert(Activity activity){
    return jdbcTemplate.update(conn -> {
		  	int idLieu = handleLocation(activity.getLieu());

			Array dateArray = conn.createArrayOf("DATE", activity.getDatesAsArray());

			PreparedStatement ps = conn.prepareStatement(INSERT_STMT);
			ps.setInt(1, activity.getId());
			ps.setString(2, activity.getNom());
			ps.setString(3, activity.getDescription());
			ps.setString(4, activity.getArrondissement());
			ps.setArray(5, dateArray);
			ps.setInt(6, idLieu);
			return ps;
		});
	}

	private static final String FIND_BY_RADIUS =
	"select activity.idactivity, activity.nom, description, arrondissement, dates, activity.idlieu from activity "+
	"inner join Lieu on activity.idLieu = lieu.idLieu "+
	"inner join (select distinct idactivity from activity, unnest(dates) as uDate where uDate >= ? and uDate <= ?) activityFilter on activity.idActivity = activityFilter.idActivity " +
	"where ST_DWithin(lieu.geo, ST_MakePoint( ? ,?), ?, false)"
	;

	public List<Activity> selectByRadius(java.sql.Date d1, java.sql.Date d2, double lat, double lng, double radius){
    return jdbcTemplate.query(FIND_BY_RADIUS,
        new PreparedStatementSetter() {
              public void setValues(PreparedStatement ps) throws
                SQLException {
                    ps.setDate(1,d1);
                    ps.setDate(2,d2);
                    ps.setDouble(3,lat);
                    ps.setDouble(4,lng);
                    ps.setDouble(5,radius);
                }
        },
        new ActivityRowMapper()
    );
	}

	//checks wether or not location exists in the db, if not inserts it.
	private int handleLocation(Location lieu){
		Location dbLieu = locationRepository.selectByContent(lieu.getNom(), lieu.getLat(), lieu.getLng());
		if(dbLieu == null){
			locationRepository.insert(lieu);
			dbLieu = locationRepository.selectByContent(lieu.getNom(), lieu.getLat(), lieu.getLng()); //TODO: Refactor that
		}

		return dbLieu.getIdLieu();
	}

	private static final String FIND_ALL_STMT =
	"select * from activity"
	;

	public List<Activity> selectAll(){
		return jdbcTemplate.query(FIND_ALL_STMT, new ActivityRowMapper());
	}

	private static final String FIND_BY_ID_STMT =
	"select * from activity " +
	"where idActivity = ?"
	;

	public Activity selectById(int id) {
    	return checkDataIntegrity(jdbcTemplate.query(FIND_BY_ID_STMT, new Object[]{id}, new ActivityRowMapper()));
  	}

	private static final String FIND_BY_NAME_STMT =
	"select * from activity " +
	"where nom = ?"
	;

	public Activity selectByName(String name) {
    	return checkDataIntegrity(jdbcTemplate.query(FIND_BY_NAME_STMT, new Object[]{name}, new ActivityRowMapper()));
  	}

  	public Activity checkDataIntegrity(List<Activity> data){
		if(		data.isEmpty()) return null;
		else if(data.size() == 1) return data.get(0);
		else throw new org.springframework.dao.EmptyResultDataAccessException(1); //Throws exception with expected size of 1
	}

	private static final String TRUNCATE_STMT =
	"truncate table activity cascade"
	;

	public void truncate()
	{
		jdbcTemplate.execute(TRUNCATE_STMT);
		locationRepository.truncate();
	}

	class ActivityRowMapper implements RowMapper<Activity> {
		public Activity mapRow(ResultSet rs, int rowNum) throws SQLException {
			Location lieu = locationRepository.selectById(rs.getInt("idLieu"));
			if(lieu != null){
				return new Activity(
					rs.getInt("idActivity"),
					rs.getString("nom"),
					rs.getString("description"),
					rs.getString("arrondissement"),
					(java.sql.Date[])rs.getArray("dates").getArray(),
					lieu
				);
			} else return null;
		}
	}
}
