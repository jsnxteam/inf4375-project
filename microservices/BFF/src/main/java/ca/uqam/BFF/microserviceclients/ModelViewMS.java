package ca.uqam.BFF.microserviceclients;

import java.util.*;

import ca.uqam.utils.interfaces.IMicroServiceClient;
import ca.uqam.utils.exceptions.*;

import org.springframework.web.client.*;
import org.springframework.http.*;
import org.springframework.http.converter.*;
import org.springframework.web.util.*;
import org.springframework.util.*;

import java.nio.charset.*;

import org.slf4j.*;

public class ModelViewMS implements IMicroServiceClient<ResponseEntity>{
	private final Logger log = LoggerFactory.getLogger(ModelViewMS.class);
	
	private String baseURI;
	private RestTemplate rt;
	private UriComponents sampleUri;

	public ModelViewMS(String baseURI){
		this.baseURI = baseURI;
		rt = new RestTemplate();
		rt.getMessageConverters()
		  .add(0,  new StringHttpMessageConverter(Charset.forName("UTF-8")));
		buildSampleURI();
	}

	private void buildSampleURI(){
		this.sampleUri = UriComponentsBuilder.fromHttpUrl(baseURI+"/")
    									     .build();
	}

	public ResponseEntity executeRequest(Map<String, String> allRequestParams) throws MicroServiceFailedException {
		return executeRequest();
	}

	public ResponseEntity executeRequest() throws MicroServiceFailedException {
		UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(baseURI+"/")
    												      .build();

    	try{
    		return rt.getForEntity(uriComponents.toUri(), String.class);
    	} catch (HttpClientErrorException e){
    		return new ResponseEntity<String>(e.getResponseBodyAsString(), e.getStatusCode());
    	} catch (Exception e){
    		log.error(e.getMessage());
			throw new MicroServiceFailedException(this);
    	}
	}


	public boolean testRequest(){
		try{
			ResponseEntity<String> resp = rt.getForEntity(sampleUri.toUri(), String.class);
			return true;
		} catch(Exception e){
			log.error("Connexion to "+sampleUri+ " failed with : " + e.getMessage());
			return false;
		}
	}

	public ResponseEntity fallback(){
		return new ResponseEntity("Application is temporarily unavailable. Sorry for the inconvenience!", HttpStatus.SERVICE_UNAVAILABLE);
	}

	public String getMicroServiceBaseURI(){
		return this.baseURI;
	}

	private MultiValueMap<String, String> mapToMultiValueMap(Map<String, String> map){
    	MultiValueMap<String, String> ret = new LinkedMultiValueMap<String, String>();
    	
    	for(Map.Entry<String, String> entry : map.entrySet())
    		ret.add(entry.getKey(), entry.getValue());

    	return ret;
    }
}