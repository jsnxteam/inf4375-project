package ca.uqam.BFF.microserviceclients;

import java.util.*;

import ca.uqam.utils.interfaces.IMicroServiceClient;
import ca.uqam.utils.exceptions.*;

import org.springframework.web.client.*;
import org.springframework.http.*;
import org.springframework.http.converter.*;
import org.springframework.web.util.*;
import org.springframework.util.*;

import java.nio.charset.*;

import org.slf4j.*;

public class ActivityMS implements IMicroServiceClient<ResponseEntity>{
	private final Logger log = LoggerFactory.getLogger(ActivityMS.class);
	
	private String baseURI;
	private RestTemplate rt;
	private UriComponents sampleUri;

	public ActivityMS(String baseURI){
		this.baseURI = baseURI;
		rt = new RestTemplate();
		rt.getMessageConverters()
		  .add(0,  new StringHttpMessageConverter(Charset.forName("UTF-8")));
		 buildSampleURI();
	}

	private void buildSampleURI(){
		//?du=2017-04-01&au=2017-04-15
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add("du", "2017-04-01");
		params.add("au", "2017-04-15");
		params.add("rayon", "5000");
		params.add("lat", "-73.123");
		params.add("lng", "25.123");

		this.sampleUri = UriComponentsBuilder.fromHttpUrl(baseURI+"/activites-375e")
    									     .queryParams(params)
    									     .build();
	}

	public ResponseEntity executeRequest(Map<String, String> allRequestParams) throws MicroServiceFailedException {
		UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(baseURI+"/activites-375e")
    												      .queryParams(mapToMultiValueMap(allRequestParams))
    												      .build();

    	try{
    		return rt.getForEntity(uriComponents.toUri(), String.class);
    	} catch (HttpClientErrorException e){
    		return new ResponseEntity<String>(e.getResponseBodyAsString(), e.getStatusCode());
    	} catch (Exception e){
    		log.error(e.getMessage());
			throw new MicroServiceFailedException(this);
    	}
	}

	public boolean testRequest(){
		try{
			ResponseEntity<String> resp = rt.getForEntity(sampleUri.toUri(), String.class);
			return true;
		} catch(Exception e){
			log.error("Connexion to "+sampleUri+ " failed with : " + e.getMessage());
			return false;
		}
	}

	public ResponseEntity fallback(){
		return new ResponseEntity("Activities are temporarily unavailable. Sorry for the inconvenience!", HttpStatus.SERVICE_UNAVAILABLE);
	}

	public String getMicroServiceBaseURI(){
		return this.baseURI;
	}

	private MultiValueMap<String, String> mapToMultiValueMap(Map<String, String> map){
    	MultiValueMap<String, String> ret = new LinkedMultiValueMap<String, String>();
    	
    	for(Map.Entry<String, String> entry : map.entrySet())
    		ret.add(entry.getKey(), entry.getValue());

    	return ret;
    }
}