package ca.uqam.BFF.controllers;

import java.util.*;
import java.io.*;

import ca.uqam.BFF.microserviceclients.*;
import ca.uqam.utils.CircuitBreaker;
import ca.uqam.utils.exceptions.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.*;
import org.springframework.http.*;
import org.springframework.http.converter.*;
import org.springframework.web.util.*;
import org.springframework.util.*;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.nio.charset.*;

import org.slf4j.*;

@Controller
public class RouterController {

	private final Logger log = LoggerFactory.getLogger(RouterController.class);


	// MS CONFIG ##########
	private final String activities_MS = "http://localhost:8081";
	private final String stationsBixi_MS = "http://localhost:8082";
    private final String model_view_MS = "http://localhost:8083";
	//#####################

    private CircuitBreaker<ResponseEntity, ActivityMS> activityCaller;
    private CircuitBreaker<ResponseEntity, BixiMS> bixiCaller;
    private CircuitBreaker<ResponseEntity, ModelViewMS> modelViewCaller;

	public RouterController(){
        this.activityCaller = new CircuitBreaker(new ActivityMS(activities_MS));
        this.bixiCaller = new CircuitBreaker(new BixiMS(stationsBixi_MS));
        this.modelViewCaller = new CircuitBreaker(new ModelViewMS(model_view_MS));
	}

	@RequestMapping("/")
	public ResponseEntity index() throws MicroServiceFailedException {
        Map<String, String> params = new HashMap<>();
        return this.modelViewCaller.executeMCCall(params);
	}

    @RequestMapping(value = {"/activites-375e/", "/activites-375e"}, method = RequestMethod.GET)
    public ResponseEntity getActivities(@RequestParam Map<String, String> allRequestParams) throws MicroServiceFailedException {
        return this.activityCaller.executeMCCall(allRequestParams);
    }

    @RequestMapping(value = {"/stations-bixi", "/stations-bixi/"}, method = RequestMethod.GET)
    public ResponseEntity getStations(@RequestParam Map<String, String> allRequestParams){
    	return this.bixiCaller.executeMCCall(allRequestParams);
    }
}
