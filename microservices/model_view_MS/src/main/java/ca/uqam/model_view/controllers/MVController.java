package ca.uqam.model_view.controllers;

import java.util.*;
import java.io.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.*;
import org.springframework.http.*;

import org.slf4j.*;

@Controller
public class MVController{

	private final Logger log = LoggerFactory.getLogger(MVController.class);

	@RequestMapping("/")
	public String index(Model model) {
		return "index";
	}
}