document.addEventListener('DOMContentLoaded', () => {
    var activitiesMarkers = new Array();
    var activitiesLatLngs = new Array();
    var stationMarkers = new Array();
    var markerCount = 0;
    var mapInstance = L.map('mapid').setView([45.508933, -73.568574], 13);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1Ijoic2FpeWFqaW4iLCJhIjoiY2o0cmQ5b3J3MnJneTMybDhkaHRkb2J6dSJ9.zCi0SszAO8APN8GjqeTUTQ'
    }).addTo(mapInstance);

    // INITIALIZATION ################################################################
    const body = document.querySelector('body');
    const activitiesContainer = document.querySelector('.activitiesContainer');

    //Link activity query form to proper handling function
    var activityQueryForm = document.querySelector('.activityQuery');
    activityQueryForm.addEventListener('submit', (e) => {
        e.preventDefault();
        var d1 = document.querySelector('.d1');
        var d2 = document.querySelector('.d2');
        queryActivity(d1.value, d2.value);
    });

    // ###############################################################################

    // ACTIVITY RENDERER #############################################################
    const renderActivity = (act) => `<li><a id="${++markerCount}" href="#"> ${act.nom}</a></li>`;

    const renderActivityList = (activities) => {
        if(activities.length > 0)
            return `<ul>${activities.map(renderActivity).join('')}</ul>`;
        return activities;
    }

    // creates an activity marker that is added to the activitiesMarkers array
    const addActivityToMarkers = (act) => {
        var marker = L.marker([act.lieu.lat,act.lieu.lng]).on('click', onMarkerClick).addTo(mapInstance);
        var popUpStr = `<b>${act.nom}</b><br><br><b>Description :</b> ${act.description}<br><br><b>Dates:</b><br><ul>`;

        for(var i = 0; i<act.dates.length;i++){
            popUpStr += `<li>${act.dates[i]}</li>`;
        }

        popUpStr += `</ul><br><b>Lieu: </b>${act.lieu.nom}`;

        marker.bindPopup(popUpStr);
        activitiesMarkers.push(marker);
        activitiesLatLngs.push([act.lieu.lat,act.lieu.lng]);
        mapInstance.addLayer(marker);
    }

    // creates the list of activities markers
    const generateActivitiesMarkers = (activities) => {
        if(activities.length > 0){
            removeActivitiesMarkers();
            activities.map(addActivityToMarkers);

            var bounds = new L.LatLngBounds(activitiesLatLngs);
            mapInstance.fitBounds(bounds);
        }
        return activities;
    }

    // removes all activities markers
    const removeActivitiesMarkers = () => {
        if(activitiesMarkers.length > 0 ){
            for(i=0;i<activitiesMarkers.length;i++) {
                mapInstance.removeLayer(activitiesMarkers[i]);
            }
            activitiesMarkers = [];
            activitiesLatLngs = [];
            markerCount = 0;
        }
    }
    // displays the html for the activity list
    const injectActivities = (activitiesHTMLList) => {
        activitiesContainer.innerHTML = activitiesHTMLList;
    }
    // displays the errors if errors were found instead of the activity list
    const injectError = (error) => {
        activitiesContainer.innerHTML = `<br>` + error;
    }
    // ###############################################################################

    // fetch activities on GET /activites-375e and renders it to the DOM
    const fetchActivities = (d1, d2, range, lat, lng) =>
        fetch(`/activites-375e?du=${ d1 }&au=${ d2 }&rayon=${ range }&lat=${ lat }&lng=${ lng }`)
            .then((resp) => handleErrors(resp))
            .then((resp) => resp.json())
            .then((resp) => generateActivitiesMarkers(resp))
            .then((resp) => injectActivities(renderActivityList(resp)))
            .catch((resp) => resp);

    // properly calls fetch function
    const queryActivity = (d1, d2) => fetchActivities(d1, d2, 5000, 45.508762, -73.568780);

    // onClick of an Activity to center on a coordinate
    $('body').delegate('a','click',function(e) {
        var id = $(this).attr('id');
        var latLngs = [ activitiesMarkers[id-1].getLatLng() ];
        var bounds = L.latLngBounds(latLngs);
        mapInstance.fitBounds(bounds);
        e.preventDefault();
    });

    // ###############################################################################

    // fetch bixis on GET /station-bixi and displays the markers around an activity marker
    const fetchStations = (lat, lng) =>
        fetch(`/stations-bixi?rayon=500&lat=${ lat }&lng=${ lng }`)
            .then((resp) => handleErrors(resp))
            .then((resp) => resp.json())
            .then((resp) => generateStationsMarkers(resp))
            .catch((resp) => { return resp; });

    // creates a station marker that is added to the stationMarkers array
    const addStationToMarkers = (sta) => {
        var marker = L.circleMarker([sta.lieu.lat,sta.lieu.lng],{radius: 8,
                                                fillOpacity: 1,
                                                color: 'black',
                                                fillColor: 'white',
                                                weight: 1,}).addTo(mapInstance);
        var popUpStr = `<b>Croisement:</b> ${sta.s} <br><b>Nombre de bixis disponibles:</b> ${sta.velosDisponibles}`;
        marker.bindPopup(popUpStr);
        stationMarkers.push(marker);
        mapInstance.addLayer(marker);
    }

    // creates the list of station markers
    const generateStationsMarkers = (stations) => {
        removeStationMarkers();
        if(stations.length > 0){
            stations.map(addStationToMarkers);
        }
        return stations;
    }

    // removes all station markers
    const removeStationMarkers = () => {
        if(stationMarkers.length > 0 ){
            for(i=0;i<stationMarkers.length;i++) {
                mapInstance.removeLayer(stationMarkers[i]);
            }
            stationMarkers = [];
        }
    }

    // show stations on marker click
    function onMarkerClick(e) {
        var markerPos = this.getLatLng();
        fetchStations(markerPos.lat, markerPos.lng);
        mapInstance.invalidateSize();
    }

    //error handling on data fetch
    const handleErrors = (response) => {
        if(!response.ok && Response.status != 500) {
            response.text().then(text => {
                injectError(text);
                return text;
            });
            throw Error(`${response.status} : ${response.statusText}`);
        }
        else if(response.status == 500){
            injectError(response.statusText);
            throw Error(`${response.status} : ${response.statusText}`);
        }
        return response;
    };
});