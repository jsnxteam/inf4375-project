## feature/a3 url tests

### valid request - return everything
[Valid no param](http://localhost:8080/activites-375e)

### valid request - return no result
[Valid no result](http://localhost:8080/activites-375e?du=2017-04-01&au=2017-04-15&rayon=3000&lat=45.123&lng=-73.123)

### valid request - date "du" missing (test with sys.out.print in ActivityController.java)
[date "du" missing ](http://localhost:8080/activites-375e?au=2017-04-15&rayon=3000&lat=45.123&lng=-73.123)

### valid request - date "au" missing (test with sys.out.print in ActivityController.java)
[date "au" missing](http://localhost:8080/activites-375e?du=2017-04-01&rayon=3000&lat=45.123&lng=-73.123)

### invalid request - missing date
[Missing date](http://localhost:8080/activites-375e?rayon=3000&lat=45.123&lng=-73.123)

### invalid request - invalid date
[Invalid date](http://localhost:8080/activites-375e?du=asd&au=weqwe&rayon=3000&lat=45.123&lng=-73.123)

### invalid request - first date isn't prior to second date.
[Invalid date order](http://localhost:8080/activites-375e?du=2017-04-15&au=2017-04-01&rayon=3000&lat=45.123&lng=-73.123)

### invalid request - invalid rayon
[Invalid rayon](http://localhost:8080/activites-375e?du=2017-04-01&au=2017-04-15&rayon=asdasd&lat=45.123&lng=-73.123)

### invalid request - invalid lat
[Invalid lat](http://localhost:8080/activites-375e?du=2017-04-01&au=2017-04-15&rayon=3000&lat=asdasd&lng=-73.123)

### invalid request - invalid lng
[Invalid lng](http://localhost:8080/activites-375e?du=2017-04-01&au=2017-04-15&rayon=3000&lat=45.123&lng=asdasd)

### valid request - missing just rayon
[Missing rayon](http://localhost:8080/activites-375e?du=2017-04-01&au=2017-04-15&lat=45.123&lng=-73.123)

### valid request - missing just lat & lng
[Missing lat & lng](http://localhost:8080/activites-375e?du=2017-04-01&au=2017-04-15&rayon=3000)

### Invalid request - missing lat but have lng
[Missing lat but have lng](http://localhost:8080/activites-375e?du=2017-04-01&au=2017-04-15&rayon=3000&lng=-73.123)

### Invalid request - missing lng but have lat
[Missing lng but have lat](http://localhost:8080/activites-375e?du=2017-04-01&au=2017-04-15&rayon=3000&lat=45.123)

### Invalid request - missing rayon, lat, lng
[Missing rayon/lat/lng](http://localhost:8080/activites-375e?du=2017-04-01&au=2017-04-15)
