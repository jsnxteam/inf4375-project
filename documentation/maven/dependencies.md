# Local Maven Dependencies - how to add them?
## Step 1 - setup a project repository
Create a repo directory at the root of the project

    mkdir repo
    
Add Repository reference to your pom.xml file

    <repositories>
        <repository>
            <id>repo</id>
            <url>file:${project.basedir}/repo</url>
        </repository>
    </repositories>
    
`Please note the "file:" which is needed in order to reference your repository as local`
`If you have already done this step, skip to Step 2`

## Step 2 - Install jar file to your local repository
From the root of your project, execute this command :

    mvn install:install-file -DlocalRepositoryPath=repo -DcreateChecksum=true -Dpackaging=jar -Dfile=[path-to-jar] -DgroupId=[...] -DartifactId=[...] -Dversion=[...]
    
for each artifact with a group id of form x.y.z Maven will include the following location inside your project's repo dir as :

    repo/
    | - x/
    |   | - y/
    |   |   | - z/
    |   |   |   | - ${artifactId}/
    |   |   |   |   | - ${version}/
    |   |   |   |   |   | - ${artifactId}-${version}.jar
    
`Some people have written scripts to automate this trivial task`

## Step 3 - Now you can add dependency as usual

Add the following to your pom.xml file : 

    <dependency>
        <groupId>[...]</groupId>
        <artifactId>[...]</artifactId>
        <version>[...]</version>
    </dependency>
