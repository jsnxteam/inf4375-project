# How to build a lib jar for your maven projects
## Step 1 - code your library as usual without a main class/method
## Step 2 - the pom.xml
Looks as such

    <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
        <modelVersion>4.0.0</modelVersion>
        <groupId>[...]</groupId>
        <artifactId>[...]</artifactId>
        <version>[...]</version>
        <dependencies>
            <dependency>[...]</dependency>
        </dependencies>
    </project>

## Step 3 - use mvn to package the library
Use the following from the root of your library project

    mvn package

`the jar file will be by default in your target folder at the root of your project`