# clone the project
`git clone https://nox282@bitbucket.org/jsnxteam/inf4375-project.git`

# switch branch
`git checkout branch_name`
> NOTE : You can't checkout another branch if there are uncommited changes in the working branch 
### How to name a branch
purpose/branch_name

# create a branch
`git checkout -b branch_name`
> NOTE : creates the branch then checks it out.

# plug branch to corresponding upstream
`git push --set-upstream origin branch_name`
> NOTE : You need to do this after you create a branch if you want to share on the distant repository, namely origin

# add files to the file tracker
`git add [files]` OR `git add .`
> NOTE : the `.` adds everything that is in the current directory, use with extra caution

# commit to local branch
`git commit [files] -m 'comment'` OR `git commit -am 'comment'`
> NOTE : Apply extra caution with -a as it will commit everything that has changed on the tracked files

# push to distant repository
`git push`

# stash
### put everything into stash
`git stash`
### delete elements that are in the stash only for the working branch
`git stash pop`
### delete everything that is in the stash
`git stash clear`
### put back elements from the stash onto the working branch
`git stash apply`
> NOTE : You can use the stash to move files between branches, or to checkout another branch without having to commit your current changes

# merge
`git merge branch_name`
> NOTE : Applies branch_name commits to working branch

# delete branch
### on local repository
`git branch -d branch_name`
### on distant repository, namely origin
`git push --delete origin branch_name`