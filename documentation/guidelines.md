# Documentation guidelines

## What needs to be documented?
- Tasks 
- Interfaces
- ReST API
- Tests
- Process

## How ?
### Tasks
In appropriate file (see `documentation/tasks.md`), specify :

    - title of the task
    - when does it run ?
    - why does it have to run ?

### Interfaces
In appropriate file (see `documentation/interfaces.md`), specify :

    - title of the Interface
    - what class should implement it ?
    - what the class should do ?

Also, one must fill in the standard [javadoc](https://www.tutorialspoint.com/java/java_documentation.htm) directly on the interface declaration.

### ReST API
In appropriate file (see `documentation/ReST-API.md`), specify as such :

| Method | Ressource (URL) |
| ------ | --------------- |
| GET | `/activites-375e` |

### Tests
Unit test should have their [javadoc](https://www.tutorialspoint.com/java/java_documentation.htm) filled in, defining what exactly they test.
### Process
Create a file per process named `[process-name].md`. Process file format does not matter as long as there is :

    - title of the process
    - short description
    - step by step how-to
    