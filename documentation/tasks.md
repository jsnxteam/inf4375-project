# Tasks
## FetchActivities.java
    Title : Fetch Activities
    When ? : every Wednesday and Sunday at 6:00 am
    Why? : to keep data updated
    Additionnal Details : You can manually update activities on `POST localhost:8081/activites-375e/updater`

## FetchBixiData.java
	Title : Fetch Bixi Stations
	When ? : once every 10 minutes
	Why ? : to keep data updated
	Additionnal Details : You can manually update bixi stations on `POST localhost:8082/staitons-bixi/updater`