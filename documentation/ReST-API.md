# Request Mapping
All mapping requests shall catch every exceptions, log them and return 500 without stacktrace
	`ResponseEntity.status(500).build();`

# ReST-API

| Method | Ressource (URL) | What it does |
| ------ | --------------- | ------------ |
| GET | `localhost:8080/` | renvoi l'application ajax |
| GET | `localhost:8080/activites-375e` | renvoi tous les activites |
| GET | `localhost:8080/activites-375e?du=[date]&au=[date]&rayon=[double]&lat=[double]&lng=[double]` | renvoi les ativites selon les parametres |
| GET | `localhost:8080/stations-bixi` | renvoi toutes les stations |
| GET | `localhost:8080/stations-bixi?dispo_bixi_min=[int]&rayon=[double]&lat=[double]&lng=[double]` | renvoi les ativites selon les parametres |
| POST | `localhost:8081/activites-375e/updater` | Lance la tache `fetch activities` |
| GET | `localhost:8081/activites-375e/id/:id` | renvoi l'activite a l'id donne |
| GET | `localhost:8081/activites-375e/name/:name` | renvoi l'activite au nom donne |
| POST | `localhost:8082/stations-bixi/updater` | Lance la tache `fetch bixi stations` |
| GET | `localhost:8082/stations-bixi/id/:id` | renvoi la station a l'id donne |
| GET | `localhost:8082/stations-bixi/name/:name` | renvoi la station au nom donne |