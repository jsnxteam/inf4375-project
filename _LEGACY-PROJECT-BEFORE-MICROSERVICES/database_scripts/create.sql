CREATE TABLE Lieu(
    idLieu SERIAL NOT NULL,
    nom TEXT NOT NULL,
    lat DOUBLE PRECISION,
    lng DOUBLE PRECISION,
    PRIMARY KEY (idLieu, nom),
    UNIQUE (idLieu)
);

SELECT AddGeometryColumn('lieu','geo',4326,'POINT',2);

CREATE TABLE Activity(
    idActivity INTEGER PRIMARY KEY UNIQUE NOT NULL,
    nom TEXT NOT NULL,
    description TEXT,
    arrondissement TEXT,
    dates DATE[],
    idLieu SERIAL REFERENCES Lieu (idLieu) NOT NULL
);

CREATE TABLE Station_Bixi(
    idStation SERIAL PRIMARY KEY UNIQUE NOT NULL,
    idBixi INTEGER NOT NULL,
    nom TEXT NOT NULL,
    idTerminal INTEGER NOT NULL,
    etat INTEGER NOT NULL,
    bloquee BOOLEAN NOT NULL,
    suspendue BOOLEAN NOT NULL,
    hors_service BOOLEAN NOT NULL,
    MAJ_Depuis BIGINT,
    COM_Depuis BIGINT,
    idLieu SERIAL REFERENCES Lieu (idLieu) NOT NULL,
    bornes_disponibles INTEGER NOT NULL,
    bornes_indisponibles INTEGER NOT NULL,
    velos_disponibles INTEGER NOT NULL,
    velos_indisponibles INTEGER NOT NULL
);