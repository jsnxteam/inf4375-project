-- #############################################################################################
-- Checks if the row to be inserted is unique
CREATE OR REPLACE FUNCTION check_nom_lieu() RETURNS trigger AS $check_nom_lieu$
	DECLARE
		similar_lieu	INTEGER;
	BEGIN
		SELECT COUNT(*) INTO similar_lieu FROM Lieu
		WHERE Lieu.nom = NEW.nom AND Lieu.lat = NEW.lat AND Lieu.lng = NEW.lng;
		IF(similar_lieu > 0) THEN
			RAISE EXCEPTION '% already exists at (%,%)', NEW.nom, NEW.lat, NEW.lng;
		END IF;
		RETURN NEW;
	END;
$check_nom_lieu$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS check_nom_lieu ON Lieu;
CREATE TRIGGER check_nom_lieu
	BEFORE INSERT ON Lieu
	FOR EACH ROW EXECUTE PROCEDURE check_nom_lieu();

-- #############################################################################################
-- Adds Geometric value on geo column to Lieu table after successful insert
CREATE OR REPLACE FUNCTION add_geo() RETURNS trigger AS $add_geo$
	DECLARE
		coordinate		TEXT;
	BEGIN
		coordinate = CONCAT('POINT(', NEW.lat, ' ', NEW.lng, ')');
		UPDATE Lieu
		SET geo = ST_GeomFromText(coordinate, 4326)
		WHERE Lieu.nom = NEW.nom AND Lieu.lat = NEW.lat AND Lieu.lng = NEW.lng;
		RETURN NULL;
	END;
$add_geo$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS add_geo ON Lieu;
CREATE TRIGGER add_geo
	AFTER INSERT ON Lieu
	FOR EACH ROW EXECUTE PROCEDURE add_geo();

-- #############################################################################################
-- Updates Geometric value on geo colunm to Lieu table after update
CREATE OR REPLACE FUNCTION update_geo() RETURNS trigger AS $update_geo$
	DECLARE
		coordinate		TEXT;
	BEGIN
		IF((NEW.lat <> OLD.lat) AND (NEW.lng <> OLD.lng)) THEN
			coordinate = CONCAT('POINT(', NEW.lat, ' ', NEW.lng, ')');
			UPDATE Lieu
			SET geo = ST_GeomFromText(coordinate, 4326)
			WHERE Lieu.nom = NEW.nom AND Lieu.lat = NEW.lat AND Lieu.lng = NEW.lng;
		END IF;
		RETURN NULL;
	END;
$update_geo$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_geo ON Lieu;
CREATE TRIGGER update_geo
	AFTER UPDATE ON Lieu
	FOR EACH ROW EXECUTE PROCEDURE update_geo();
-- #############################################################################################