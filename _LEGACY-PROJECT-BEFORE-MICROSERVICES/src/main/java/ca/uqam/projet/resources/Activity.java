package ca.uqam.projet.resources;

import com.fasterxml.jackson.annotation.*;
import ca.uqam.projet.resources.Location;
import java.io.*;
import java.util.*;

public class Activity{
	@JsonProperty("id")
	private int id;

	@JsonProperty("nom")
	private String nom;

	@JsonProperty("description")
	private String description;

	@JsonProperty("arrondissement")
	private String arrondissement;

	@JsonProperty("dates")
	private List<String> dates;

	@JsonProperty("lieu")
	private Location lieu;

	@Override
	public String toString(){
		return String.format("%s : %s", nom, lieu.toString());
	}

	public Activity(){}

	public Activity(int id, String nom, String description, String arrondissement, java.sql.Date[] dates, Location lieu) {
		this.id = id;
		this.nom= changeToUTF8(nom);
		this.description = changeToUTF8(description);
		this.arrondissement = arrondissement;
		setDateList(dates);
		this.lieu = lieu;
	}

	private void setDateList(java.sql.Date[] dates){
		this.dates = new ArrayList<String>();
		
		for(int i = 0; i < dates.length; i++)
			this.dates.add(dates[i].toString());
	}

	private String changeToUTF8(String toChange){
		byte[] bStr;
		String fStr = "";
		try{
		 	bStr = toChange.getBytes("UTF-8");
			fStr = new String(bStr, "UTF-8");
		}catch(UnsupportedEncodingException e){}
		return fStr;

	}

	@JsonProperty public int getId() { return this.id; }
	@JsonProperty public String getNom() { return this.nom; }
	@JsonProperty public String getDescription() { return this.description; }
	@JsonProperty public String getArrondissement() { return this.arrondissement; }
	@JsonProperty public Location getLieu() {return this.lieu; }
	@JsonProperty public List<String> getDates() { return this.dates; }

	public String[] getDatesAsArray(){
		if(this.dates.isEmpty())
			return new String[]{"Unknown Date"};
		
		return this.dates.toArray(new String[this.dates.size()]);
	}
}