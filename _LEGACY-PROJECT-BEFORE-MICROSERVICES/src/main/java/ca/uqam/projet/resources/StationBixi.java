package ca.uqam.projet.resources;

import com.fasterxml.jackson.annotation.*;
import ca.uqam.projet.resources.Location;

import java.math.BigInteger;

public class StationBixi{
	@JsonProperty("id")
	private int id;

	@JsonProperty("s")
	private String nom;
	
	@JsonProperty("n")
	private int idTerminale;
	
	@JsonProperty("st")
	private int etat;
	
	@JsonProperty("b")
	private boolean bloquee;
	
	@JsonProperty("su")
	private boolean suspendue;
	
	@JsonProperty("m")
	private boolean hors_service;
	
	@JsonProperty("lu")
	private long maj_depuis;
	
	@JsonProperty("lc")
	private long com_depuis;
	
	@JsonProperty("la")
	private double latitude;

	@JsonProperty("lo")
	private double longitude;

	@JsonProperty("da")
	private int bornes_disponibles;

	@JsonProperty("dx")
	private int bornes_indisponibles;

	@JsonProperty("ba")
	private int velos_disponibles;

	@JsonProperty("bx")
	private int velos_indisponibles;

	private Location lieu;

	@JsonProperty("bk")
	private boolean bk;	//unused

	@JsonProperty("bl")
	private boolean bl; //unused

	public StationBixi(){}
	
	public StationBixi(int id, String nom, int idTerminale, int etat, boolean bloquee, boolean suspendue, boolean hors_service, long maj_depuis, long com_depuis, double latitude, double longitude, int bornes_disponibles, int bornes_indisponibles, int velos_disponibles, int velos_indisponibles, 	Location lieu){
		this.id = id;
		this.nom = nom;
		this.idTerminale = idTerminale;
		this.etat = etat;
		this.bloquee = bloquee;
		this.suspendue = suspendue;
		this.hors_service = hors_service;
		this.maj_depuis = maj_depuis;
		this.com_depuis = com_depuis;
		this.latitude = latitude;
    	this.longitude = longitude;
    	this.bornes_disponibles = bornes_disponibles;
    	this.bornes_indisponibles = bornes_indisponibles;
    	this.velos_disponibles = velos_disponibles;
    	this.velos_indisponibles = velos_indisponibles;
   		this.lieu = lieu;
	}

	public void finishSerialization(){
		this.lieu = new Location("Station "+this.nom, this.latitude, this.longitude);
	}

	@JsonProperty public int getId() { return this.id; }
	@JsonProperty public String getNom() { return this.nom; }
	@JsonProperty public int getIdTerminale() { return this.idTerminale; }
	@JsonProperty public int getEtat() { return this.etat; }
	@JsonProperty public boolean isBloquee() { return this.bloquee; }
	@JsonProperty public boolean isSuspendue() { return this.suspendue; }
	@JsonProperty public boolean isHorsService() { return this.hors_service; }
	@JsonProperty public long getMajDepuis() { return this.maj_depuis; }
	@JsonProperty public long getComDepuis() { return this.com_depuis; }
	@JsonProperty public Location getLieu() { return this.lieu; }
	@JsonProperty public double getLatitude() { return this.latitude; }
	@JsonProperty public double getLongitude() { return this.longitude; }
	@JsonProperty public int getBornesDisponibles() { return this.bornes_disponibles; }
	@JsonProperty public int getBornesIndisponibles() { return this.bornes_indisponibles; }
	@JsonProperty public int getVelosDisponibles() { return this.velos_disponibles; }
	@JsonProperty public int getVelosIndisponibles() { return this.velos_indisponibles; }

}