package ca.uqam.projet.controllers;

import java.util.*;
import java.io.*;

import ca.uqam.projet.repositories.*;
import ca.uqam.projet.resources.*;
import ca.uqam.projet.tasks.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

@Controller
public class ApplicationController {

	@Autowired private FetchBixiData bixiFetcher;

 	@RequestMapping("/")
	public String index(Model model) {
		return "index";
	}
}