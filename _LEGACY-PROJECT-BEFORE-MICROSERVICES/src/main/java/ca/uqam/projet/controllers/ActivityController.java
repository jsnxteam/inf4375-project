package ca.uqam.projet.controllers;

import java.util.*;
import java.io.*;
import java.sql.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import ca.uqam.projet.repositories.*;
import ca.uqam.projet.resources.*;
import ca.uqam.projet.tasks.FetchActivities;


import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.nio.charset.StandardCharsets;

import org.slf4j.*;

@Controller
public class ActivityController{

    @Autowired ActivityRepository activitiesRepository;
    @Autowired FetchActivities activityFetcher;
    private final Logger log = LoggerFactory.getLogger(ActivityController.class);

    @RequestMapping(value = {"/activites-375e/", "/activites-375e"}, method = RequestMethod.GET)
    public ResponseEntity getActivities(@RequestParam Map<String, String> allRequestParams){
        try{
            return findByRadius(allRequestParams);
        } catch(Exception e){
            log.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }

    }

    private ResponseEntity findByRadius(Map<String,String> allRequestParams){
       double radius = 5000, lat = 45.508762, lng = -73.568780;
       LocalDate d1 = null, d2 = null;
       boolean dontParseRadius = false, dontParseCoordinates = false, firstDatePresent = false, secondDatePresent = false;
       byte[] data;
       if(allRequestParams.get("du") == null &&  allRequestParams.get("au") == null && allRequestParams.get("rayon") == null
           && allRequestParams.get("lat") == null && allRequestParams.get("lng") == null){
           data = search();
       } else {
           if(allRequestParams.get("du") != null)
               firstDatePresent = true;

           if(allRequestParams.get("au") != null)
               secondDatePresent = true;

           if(!firstDatePresent && !secondDatePresent){
               return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The date wasn't provided.");
           }

           if (allRequestParams.get("rayon") == null && allRequestParams.get("lat") != null && allRequestParams.get("lng") != null)
               dontParseRadius = true;
           else if(allRequestParams.get("rayon") != null && allRequestParams.get("lat") == null && allRequestParams.get("lng") == null)
               dontParseCoordinates = true;
           else if(allRequestParams.get("lat") == null && allRequestParams.get("lng") != null)
               return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Latitude needs to be provided alongside the longitude.");
           else if(allRequestParams.get("lat") != null && allRequestParams.get("lng") == null)
               return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Longitude needs to be provided alongside the latitude.");
           else if(allRequestParams.get("rayon") == null && allRequestParams.get("lat") == null && allRequestParams.get("lng") == null)
               return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Rayon, lat and lng values weren't provided.");

           if(!dontParseRadius) {
               try {
                   radius = Double.parseDouble(allRequestParams.get("rayon"));
                   if(radius < 0)
                       radius = 5000;
               } catch(NumberFormatException e){
                   return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Value provided for rayon is incorrect.");
               }
           }

           if(!dontParseCoordinates) {
               try {
                   lat = Double.parseDouble(allRequestParams.get("lat"));
               } catch(NumberFormatException e){
                   return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Value provided for lat is incorrect.");
               }

               try {
                   lng = Double.parseDouble(allRequestParams.get("lng"));
               } catch(NumberFormatException e){
                   return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Value provided for lng is incorrect.");
               }
           }

           try {
               DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
               if(!firstDatePresent){
                   d2 = LocalDate.parse(allRequestParams.get("au"));
                   d1 = d2.minusDays(1);
               }
               else if(!secondDatePresent){
                   d1 = LocalDate.parse(allRequestParams.get("du"));
                   d2 = d1.plusDays(1);
               }
               else if (firstDatePresent && secondDatePresent){
                   d1 = LocalDate.parse(allRequestParams.get("du"));
                   d2 = LocalDate.parse(allRequestParams.get("au"));
               }

           }catch (DateTimeParseException dtfe) {
               return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Value provided for the date range is incorrect.");
           }

           if(!checkDateOrder(d1,d2))
               return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("The first date needs to be prior to second date.");
           data = search(d1,d2,radius,lat,lng);
       }

       return ResponseEntity.ok(new String(data, StandardCharsets.UTF_8));
   }

   private byte[] search()
   {
       byte[] data;
       List<Activity> activities = activitiesRepository.selectAll();
       ByteArrayOutputStream out = new ByteArrayOutputStream();
       ObjectMapper mapper = new ObjectMapper();
       try{
           mapper.writeValue(out, activities);
       }catch(IOException e){}

       data = out.toByteArray();
       return data;
   }
    private byte[] search(LocalDate d1, LocalDate d2, double radius, double lat, double lng){
        byte[] data;
        List<Activity> activities = activitiesRepository.selectByRadius(java.sql.Date.valueOf(d1), java.sql.Date.valueOf(d2), lat, lng, radius);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        try{
            mapper.writeValue(out, activities);
        }catch(IOException e){}

        data = out.toByteArray();
        return data;
    }

    private boolean checkDateOrder(LocalDate d1, LocalDate d2){
        return d1.isBefore(d2);
    }

    private String serializeActivities(List<Activity> activities){
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        ObjectMapper mapper = new ObjectMapper();

        try{
            mapper.writeValue(out, activities);
        }catch(IOException e){}

        return new String(out.toByteArray());
    }

    private String serializeActivity(Activity activity) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(activity);
    }

    //manualy fetch acitivities
    @RequestMapping("/activites-375e/updater")
    public ResponseEntity updateActivities(Model model) throws ProcessingException, IOException {
        try{
            activityFetcher.execute();
            return ResponseEntity.ok("OK");
        } catch (Exception e){
            log.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }
    }

    @RequestMapping("/activites-375e/id/{id}")
    public ResponseEntity selectActivityById(@PathVariable("id") int id) throws JsonProcessingException {
        try {
            Activity activity = activitiesRepository.selectById(id);
            if(activity == null)
                return ResponseEntity.notFound().build();
            return ResponseEntity.ok(serializeActivity(activity));
        } catch(Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }
    }

    @RequestMapping("/activites-375e/name/{name}")
    public ResponseEntity selectActivityByName(@PathVariable("name") String name) throws JsonProcessingException {
        try {
            Activity activity = activitiesRepository.selectByName(name);
            if(activity == null)
                return ResponseEntity.notFound().build();
            return ResponseEntity.ok(serializeActivity(activity));
        } catch (Exception e){
            log.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }
    }
}
