package ca.uqam.projet.exceptions;

import ca.uqam.projet.utils.JsonSchemaHandler;
import java.io.*;

public class JsonSchemaOverrideException extends Exception {
	private final String message = "JsonSchemaHandler already have this Json-Schema linked to it : ";
	private JsonSchemaHandler jsh;

	public JsonSchemaOverrideException(JsonSchemaHandler jsh){
		this.jsh = jsh;
	}

	@Override
	public String getMessage(){
		return message + jsh.getSchemaResource();
	}
}