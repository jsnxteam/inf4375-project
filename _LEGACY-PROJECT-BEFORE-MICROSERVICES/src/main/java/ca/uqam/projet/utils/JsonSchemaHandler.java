package ca.uqam.projet.utils;

import ca.uqam.projet.exceptions.JsonSchemaOverrideException;

import java.util.*;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ArrayNode;

import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.main.*;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.*;

import org.springframework.stereotype.*;

import org.slf4j.*;

public class JsonSchemaHandler {

	private final Logger log = LoggerFactory.getLogger(JsonSchemaHandler.class);

	private String schemaResource;
	private JsonSchema runtimeSchema;

	public JsonSchemaHandler(){}

	public JsonSchemaHandler(String ressourceURL) throws ProcessingException {
		this.runtimeSchema = JsonSchemaFactory.byDefault().getJsonSchema(ressourceURL);
	}

	/**
	 * Sets a json Schema if none have been set at initialization
	 * @throws JsonSchemaOverrideException if a json schema was already linked to the object
	 */
	public void setJsonSchema(String ressourceURL) throws JsonSchemaOverrideException, ProcessingException {
		if(this.runtimeSchema != null)
			throw new JsonSchemaOverrideException(this);
		else
			this.runtimeSchema = JsonSchemaFactory.byDefault().getJsonSchema(ressourceURL);
	}

	/**
	 * Entry Point of the Schema validation process
	 * TODO: have another entry point that takes JsonNode as argument
	 * @param  data  data in form of Array Node 
	 * @return validated JsonNode. If required field are missing they are substracted from data.
	 * @throws ProcessingException on fatal processing failure or fata Json-schema error
	 */
	public JsonNode validate(ArrayNode data) throws ProcessingException {
		ProcessingReport report = this.runtimeSchema.validate(data);
		if(!report.isSuccess())
			return handleProcessingReport(data);
		return data;
	}

	private JsonNode handleProcessingReport(ArrayNode data) throws ProcessingException {
		ProcessingReport report = this.runtimeSchema.validate(data);
		if(report.isSuccess())
			return data;
		else {
			int errorIndex = getErrorIndex(report);
	
			if(errorIndex != -1) data.remove(errorIndex);
			else throw new ProcessingException("Bad instance pointer format");

			return handleProcessingReport(data);
		}
	}

	private int getErrorIndex(ProcessingReport report) throws ProcessingException {
		Iterator it = report.iterator();
		JsonNode message;

		while(it.hasNext()){
			message = ((ProcessingMessage)it.next()).asJson();

			if(message.get("level").asText().equals("warning"))
				log.warn("Json-Schema warning : " + message.get("message").toString());
			else if(message.get("level").asText().equals("error") && message.get("keyword").asText().equals("required"))
				return getInstanceIndex(message.get("instance").get("pointer").asText());
			else if(message.get("level").asText().equals("error")) 
				throw new ProcessingException(message.get("message").toString());
		}
		return -1;
	}

	private int getInstanceIndex(String pointer){
		String[] xs = pointer.split("/");
		try{
			return Integer.parseInt(xs[1]);
		} catch (NumberFormatException e){
			return -1;
		} catch (ArrayIndexOutOfBoundsException e){
			return -1;
		}
	}

	/**
	 * @return schema ressource URI
	 */
	public String getSchemaResource() { return this.schemaResource; }

}