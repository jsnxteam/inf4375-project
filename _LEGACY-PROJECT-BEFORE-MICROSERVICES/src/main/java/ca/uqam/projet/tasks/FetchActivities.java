package ca.uqam.projet.tasks;

import ca.uqam.projet.resources.*;
import ca.uqam.projet.repositories.*;
import ca.uqam.projet.utils.*;

import java.util.*;
import java.util.stream.*;

import java.io.*;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ArrayNode;

import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.scheduling.annotation.*;
import org.springframework.web.client.*;
import org.springframework.http.converter.*;

import java.nio.charset.*;

import org.slf4j.*;

@Component
public class FetchActivities {

  	private final Logger log = LoggerFactory.getLogger(FetchActivities.class);

	private static final String URL = "http://guillemette.org/uqam/inf4375-e2017/assets/programmation-parcs.json";
	String activitySchemaResource = "resource:/JSON-Schema/Activity-Schema.json";

 	@Autowired private ActivityRepository activityRepository;
    @Autowired private StationBixiRepository stationRepository;

	@Scheduled(cron="0 0 6 * * SUN,WED") // a 6AM le Dimanche et le Mercredi
	public void execute() throws ProcessingException, IOException {
		RestTemplate rt = new RestTemplate();

		rt.getMessageConverters()
		  .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

		final String json = rt.getForObject(URL, String.class);

		final ArrayNode urlRep = (ArrayNode) JsonLoader.fromString(json);

		try{

			JsonSchemaHandler jsh = new JsonSchemaHandler(activitySchemaResource);

    		final JsonNode processedData = jsh.validate(urlRep.deepCopy());

	    	ObjectMapper mapper = new ObjectMapper();
	    	List<Activity> activities = mapper.readValue(processedData.toString(), mapper.getTypeFactory()
	        																          .constructCollectionType(List.class, Activity.class));

            activityRepository.truncate();
            stationRepository.truncate();

			for(Activity act : activities)
				activityRepository.insert(act);

			log.info("Received Json data from "+URL);
		} catch(ProcessingException e){
			log.error("Received invalid Json Format from : "+URL+"\n"+e.getMessage());
		}
	}
}
