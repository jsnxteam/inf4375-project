# Projet de démarrage

## Prérequis

- Java 1.8+
- Maven 3.0+
- PostgreSQL 9.4+ avec PostGIS 2.0+
- lib ca.uqam.utils (fournie)

## PostgreSQL

- [Installation de PostgreSQL et de PostGIS pour Windows](http://www.bostongis.com/PrinterFriendly.aspx?content_name=postgis_tut01)
- [PostgreSQL.app pour OSX](http://postgresapp.com/)

## Configuration des bases de donnees

### Pour le projet _LEGACY-PROJECT-BEFORE-MICROSERVICES/

- nom BD : inf4375_db
- user : postgres
- pass : postgres
- recquiert le plugin postgis

3 scripts sont disponnibles : 

- Creer le schema `_LEGACY-PROJECT-BEFORE-MICROSERVICES/database_scripts/create.sql`
- Creer les triggers `_LEGACY-PROJECT-BEFORE-MICROSERVICES/database_scripts/triggers.sql`
- Drop le schema `_LEGACY-PROJECT-BEFORE-MICROSERVICES/database_scripts/drop.sql`

### Pour les microservices
### Service microservices/activities_MS/

- nom BD : inf4375_activities
- user : postgres
- pass : postgres
- recquiert le plugin postgis

3 scripts sont disponnibles : 

- Creer le schema `microservices/activies_MS/database_scripts/create.sql`
- Creer les triggers `microservices/activies_MS/database_scripts/triggers.sql`
- Drop le schema `microservices/activies_MS/database_scripts/drop.sql`

### Service microservices/bixi_MS/

- nom BD : inf4375_bixi
- user : postgres
- pass : postgres
- recquiert le plugin postgis

3 scripts sont disponnibles : 

- Creer le schema `microservices/bixi_MS/database_scripts/create.sql`
- Creer les triggers `microservices/bixi_MS/database_scripts/triggers.sql`
- Drop le schema `microservices/bixi_MS/database_scripts/drop.sql`

> La version Node.js de Bixi_MS (`microservices/bixi_MS_node/`) utilise la meme base de donnees que Bixi_MS

## Installation du projet

### lib ca.uqam.utils
Plusieurs microservices on pour dependence la librarie se trouvant dans `utils-project`. Voici les etapes a suivre pour compiler la librairie et l'installer dans les differents microservices :

#### Etape 1 : build la librarie

Dans `utils-project/`

	executer `mvn package`

> le jar `utils-1.0.jar` se trouve dans `utils-project/target/`

#### Etape 2 : Installer la librairie dans les projets des microservices

> NB : le comportement par defaut de maven apres l'execution des commandes suivantes copiera le fichier `utils-1.0.jar` dans `~/.m2/repository/ca/uqam/utils/1.0/`. Pour desinstaller la librairie, il suffit de supprimer ce dossier.

> NB : dans les commandes suivante, veuillez remplacer `[full-path-to-project]` par le chemin absolu vers la racine du projet.

dans `microservices/BFF/`

    executer `mkdir repo`
             `mvn install:install-file -DlocalRepositoryPath=repo -DcreateChecksum=true -Dpackaging=jar -Dfile=[full-path-to-project]/utils-project/target/utils-1.0.jar -DgroupId=ca.uqam -DartifactId=utils -Dversion=1.0`
             
dans `microservices/activities_MS/`

    executer `mkdir repo`
             `mvn install:install-file -DlocalRepositoryPath=repo -DcreateChecksum=true -Dpackaging=jar -Dfile=[full-path-to-project]/utils-project/target/utils-1.0.jar -DgroupId=ca.uqam -DartifactId=utils -Dversion=1.0`
             
dans `microservices/bixi_MS/`

    executer `mkdir repo`
             `mvn install:install-file -DlocalRepositoryPath=repo -DcreateChecksum=true -Dpackaging=jar -Dfile=[full-path-to-project]/utils-project/target/utils-1.0.jar -DgroupId=ca.uqam -DartifactId=utils -Dversion=1.0`

#### BONUS : Installer la version Node.js du service Bixi_MS

dans `microservices/bixi_MS_node/`

    executer `npm update`

## Compilation et exécution

Pour executer le service BFF, dans `microservices/BFF/`

    executer `mvn spring-boot:run`
             
Pour executer le service activities_MS, dans `microservices/activities_MS/`

    executer `mvn spring-boot:run`
             
pour executer le service bixi_MS (version java), dans `microservices/bixi_MS/`

    executer executer `mvn spring-boot:run`

pour executer le service bixi_MS (version Node.js). dans `microservices/bixi_MS_node/`

    executer `npm start`
    
pour executer le service de modele et vue, dans `microservices/model_view_MS/`

    executer `mvn spring-boot:run`

> Le projet est alors disponible à l'adresse [http://localhost:8080/](http://localhost:8080/)

## Routes disponibles

- [http://localhost:8080/](http://localhost:8080/)

## URL utiles

| Method | Ressource (URL) | What it does |
| ------ | --------------- | ------------ |
| POST | `localhost:8081/activites-375e/updater` | Lance la tache `fetch activities` |
| GET | `localhost:8081/activites-375e/id/:id` | renvoi l'activite a l'id donne |
| GET | `localhost:8081/activites-375e/name/:name` | renvoi l'activite au nom donne |
| POST | `localhost:8082/stations-bixi/updater` | Lance la tache `fetch bixi stations` |
| GET | `localhost:8082/stations-bixi/id/:id` | renvoi la station a l'id donne |
| GET | `localhost:8082/stations-bixi/name/:name` | renvoi la station au nom donne |

Pour plus d'informations, des fichiers de documentations sont disponnible dans `documentations/`